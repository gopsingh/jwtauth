const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    avatar: {
        type: String
    }
});

const User = mongoose.model('users', UserSchema);

module.exports = User;
//The name, email, and password come from the React.js form. We fetch the avatar from gravatar based on email address.