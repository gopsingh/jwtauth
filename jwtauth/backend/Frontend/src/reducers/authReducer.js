import { SET_CURRENT_USER } from '../actions/types';
import isEmpty from '../is-empty';

const initialState = {
    isAuthenticated: false,
    user: {},
    token: null
}

export default function(state = initialState, action ) {    
    switch(action.type) {
        case SET_CURRENT_USER:
            return {
                ...state,
                isAuthenticated: !isEmpty(action.payload.decoded),
                user: action.payload.decoded,
                token: action.payload.token
            }
        default: 
            return state;
    }
}

